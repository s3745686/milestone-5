#!/bin/bash
# find every word 'de' and print one per line, then count the lines
grep '\bde\b' -io RUG_wiki_page.txt | wc -l
